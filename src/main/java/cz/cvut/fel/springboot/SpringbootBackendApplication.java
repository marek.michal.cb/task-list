package cz.cvut.fel.springboot;

import cz.cvut.fel.springboot.entity.Todo;
import cz.cvut.fel.springboot.repository.TodoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBackendApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackendApplication.class, args);
	}

	private TodoRepository todorepository;

	@Override
	public void run(String... args) throws Exception {
	}
}
