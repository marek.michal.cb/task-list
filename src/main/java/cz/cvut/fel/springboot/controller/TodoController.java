package cz.cvut.fel.springboot.controller;

import cz.cvut.fel.springboot.entity.Todo;
import cz.cvut.fel.springboot.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
@CrossOrigin("http://localhost:8081/")
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    // Get all Todos
    @GetMapping("/todos")
    public List<Todo> fetchTodos() {
        return todoRepository.findAll();
    }

    /* Create model */
    @PostMapping("/todos")
    public Todo createTodo(@RequestBody Todo todo) {
        return todoRepository.save(todo);
    }

    // Update model description
    @PostMapping ("/todos/{id}")
    public ResponseEntity<Todo> updateDescription(@PathVariable Long id, @RequestBody Todo todoDetails) {
        Todo todo = todoRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Todo does not exist with id: " + id));

        todo.setDescription(todoDetails.getDescription());

        Todo updatedTodo = todoRepository.save(todo);
        return ResponseEntity.ok(updatedTodo);
    }

    // Update model done
    @PatchMapping ("/todos/{id}")
    public ResponseEntity<Todo> updateDone(@PathVariable Long id, @RequestBody Todo todoDetails) {
        Todo todo = todoRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Todo does not exist with id: " + id));

        todo.setDone(todoDetails.isDone());

        Todo updatedTodo = todoRepository.save(todo);
        return ResponseEntity.ok(updatedTodo);
    }

    // Delete model
    @DeleteMapping("/todos/{id}")
    public ResponseEntity<Boolean> deleteTodo(@PathVariable Long id) {
        Todo todo = todoRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Todo does not exist with id: " + id));

        todoRepository.delete(todo);
        return ResponseEntity.ok(true);
    }



}
