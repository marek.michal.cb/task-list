package cz.cvut.fel.springboot.repository;

import cz.cvut.fel.springboot.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Long> {
}
